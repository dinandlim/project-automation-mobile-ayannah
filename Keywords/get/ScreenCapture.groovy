
package get

import javax.imageio.ImageIO
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File
import org.openqa.selenium.WebDriver
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.testdata.DBData as DBData
import ru.yandex.qatools.ashot.*

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class Screencapture {



	KeywordLogger log = new KeywordLogger();



	@Keyword
	public void getEntirePageMobile(String fileName) {

		Mobile.takeScreenshot(getImgPath(fileName), FailureHandling.STOP_ON_FAILURE)
	}

	@Keyword
	public void getScreenShot(String fileName) {

		File myDirectory = new File("ScreenShot/");
		if(!myDirectory.exists()) {
			myDirectory.mkdirs();
		}else{
			//Directory already exist
		}
		//WebDriver driver = DriverFactory.getWebDriver();
		//Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
		Robot robotClassObject = new Robot();

		// Get screen size
		Rectangle screenSize = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());

		// Capturing screenshot by providing size
		BufferedImage tmp = robotClassObject.createScreenCapture(screenSize);

		//filePath = getImgPath(fileName);
		//String path = "Users⁩/⁨ferdinandtaslim⁩/Documents⁩/project-automation-web-ut⁩/ScreenShot/";
		String path = getImgPath(fileName)

		//ImageIO.write(screenshot.getImage(), "JPG", new File(path));
		ImageIO.write(tmp, "JPG", new File(path));

		log.logPassed("Taking screenshot successfully\n[[ATTACHMENT|"+path+"]]");
	}
	private String getImgPath(String fileName){

		String format = ".JPG"

		if(fileName.equals("")){
			fileName = new Date().getTime().toString() + format.toLowerCase();
		}else{
			if(!fileName.toLowerCase().contains(format.toLowerCase())){
				fileName = (fileName + format).toLowerCase()
			}else{
				fileName = fileName.toLowerCase()
			}
		}

		String output = "ScreenShot/" + fileName

		return output;

		//KeywordLogger logging = KeywordLogger.getInstance();
		//String reportFolder = logging.getLogFolderPath();

		//return reportFolder+File.separator+fileName;
	}
}