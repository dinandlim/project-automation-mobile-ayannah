import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Button - Jenis Pekerjaan'), 
    0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Text - Pilihan Jenis Pekerjaan'), 
    0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Selected - Jenis Pekerjaan', 
        [('pilih_jenis_pekerjaan') : jenis_pekerjaan]), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - EmployeeId'), 
    no_induk, 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Nama Instansi'), 
    nama_instansi, 0)

Mobile.scrollToText('No telepon kantor', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Lama Bekerja'), 
    lama_bekerja, 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Alamat Kantor'), 
    alamat_kantor, 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - No Telp Kantor'), 
    no_telp, 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Nama Atasan'), 
    nama_atasan, 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Jabatan'), 
    jabatan, 0)

Mobile.scrollToText('Sumber pendapatan lain', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Gaji Perbulan'), 
    gaji_perbulan, 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Pendapatan Lain'), 
    pendapatan_lain, 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Sumber Pendapatan Lain'), 
    sumber_pendapatan, 0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Button - Selanjutnya'), 
    0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Tambahan Page/Text - Data Tambahan Lain-lain'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Tambahan Page/Text - Data Tambahan Lain-lain'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Tambahan Page/Textfield - Related Person'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Tambahan Page/Button - Status Hubungan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Tambahan Page/Textfield - Alamat'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Tambahan Page/Textfield - NoTelp'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Tambahan Page/Textfield - NoHP'), 0)

