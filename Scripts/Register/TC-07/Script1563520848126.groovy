import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Integer intBulanLahir

Integer intBulanLahirP

Integer intCurrentMonth

String[] splitDateofBirth = tanggal_lahir.split(' ')

String TanggalLahir = splitDateofBirth[0]

String BulanLahir = splitDateofBirth[1]

String TahunLahir = splitDateofBirth[2]

String[] splitDateofBirth2 = tanggal_lahir_pasangan.split(' ')

String TanggalLahirP = splitDateofBirth2[0]

String BulanLahirP = splitDateofBirth2[1]

String TahunLahirP = splitDateofBirth2[2]

Integer intTanggalLahir = Integer.parseInt(TanggalLahir)

Integer intTahunLahir = Integer.parseInt(TahunLahir)

Integer intTanggalLahirP = Integer.parseInt(TanggalLahirP)

Integer intTahunLahirP = Integer.parseInt(TahunLahirP)

if (BulanLahir.equals('Jan')) {
    intBulanLahir = '1'
} else if (BulanLahir.equals('Feb')) {
    intBulanLahir = '2'
} else if (BulanLahir.equals('Mar')) {
    intBulanLahir = '3'
} else if (BulanLahir.equals('Apr')) {
    intBulanLahir = '4'
} else if (BulanLahir.equals('May')) {
    intBulanLahir = '5'
} else if (BulanLahir.equals('Jun')) {
    intBulanLahir = '6'
} else if (BulanLahir.equals('Jul')) {
    intBulanLahir = '7'
} else if (BulanLahir.equals('Aug')) {
    intBulanLahir = '8'
} else if (BulanLahir.equals('Sep')) {
    intBulanLahir = '9'
} else if (BulanLahir.equals('Oct')) {
    intBulanLahir = '10'
} else if (BulanLahir.equals('Nov')) {
    intBulanLahir = '11'
} else if (BulanLahir.equals('Dec')) {
    intBulanLahir = '12'
}

//Untuk bulan lahir Pasangan
if (BulanLahir.equals('Jan')) {
    intBulanLahirP = '1'
} else if (BulanLahirP.equals('Feb')) {
    intBulanLahirP = '2'
} else if (BulanLahirP.equals('Mar')) {
    intBulanLahirP = '3'
} else if (BulanLahirP.equals('Apr')) {
    intBulanLahirP = '4'
} else if (BulanLahirP.equals('May')) {
    intBulanLahirP = '5'
} else if (BulanLahirP.equals('Jun')) {
    intBulanLahirP = '6'
} else if (BulanLahirP.equals('Jul')) {
    intBulanLahirP = '7'
} else if (BulanLahirP.equals('Aug')) {
    intBulanLahirP = '8'
} else if (BulanLahirP.equals('Sep')) {
    intBulanLahirP = '9'
} else if (BulanLahirP.equals('Oct')) {
    intBulanLahirP = '10'
} else if (BulanLahirP.equals('Nov')) {
    intBulanLahirP = '11'
} else if (BulanLahirP.equals('Dec')) {
    intBulanLahirP = '12'
}

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Nama KTP'), nama_ktp, 0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Jenis Kelamin', [('pilih_jenis_kelamin') : jenis_kelamin]), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Tanggal Lahir'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Layout - Month'), 
    0)

CurrentMonth = Mobile.getText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Text - Month'), 
    0)

CurrentDay = Mobile.getText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Text - Day'), 
    0)

CurrentYear = Mobile.getText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Text - Year'), 
    0)

if (CurrentMonth.equals('Jan')) {
    intCurrentMonth = '1'
} else if (CurrentMonth.equals('Feb')) {
    intCurrentMonth = '2'
} else if (CurrentMonth.equals('Mar')) {
    intCurrentMonth = '3'
} else if (CurrentMonth.equals('Apr')) {
    intCurrentMonth = '4'
} else if (CurrentMonth.equals('May')) {
    intCurrentMonth = '5'
} else if (CurrentMonth.equals('Jun')) {
    intCurrentMonth = '6'
} else if (CurrentMonth.equals('Jul')) {
    intCurrentMonth = '7'
} else if (CurrentMonth.equals('Aug')) {
    intCurrentMonth = '8'
} else if (CurrentMonth.equals('Sep')) {
    intCurrentMonth = '9'
} else if (CurrentMonth.equals('Oct')) {
    intCurrentMonth = '10'
} else if (CurrentMonth.equals('Nov')) {
    intCurrentMonth = '11'
} else if (CurrentMonth.equals('Dec')) {
    intCurrentMonth = '12'
}

Integer intCurrentDay = Integer.parseInt(CurrentDay)

Integer intCurrentYear = Integer.parseInt(CurrentYear)

if (intCurrentMonth < intBulanLahir) {
    for (int i = intCurrentMonth; i < intBulanLahir; i++) {
        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - IncrementMonth'), 
            0)
    }
} else if (intCurrentMonth > intBulanLahir) {
    for (int i = intCurrentMonth; i > intBulanLahir; i--) {
        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - DecrementMonth'), 
            0)
    }
} else {
    CustomKeywords.'set.MarkAndMessage.markPassed'('Current month dan bulan lahir sudah sama')
}

Mobile.delay(1)

if (intCurrentDay < intTanggalLahir) {
    for (int i = intCurrentDay; i < intTanggalLahir; i++) {
        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - IncrementDay'), 
            0)
    }
} else if (intCurrentDay > intTanggalLahir) {
    for (int i = intCurrentDay; i > intTanggalLahir; i--) {
        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - DecrementDay'), 
            0)
    }
} else {
    CustomKeywords.'set.MarkAndMessage.markPassed'('Current day dan tanggal lahir sudah sama')
}

if (intCurrentYear < intTahunLahir) {
    for (int i = intCurrentYear; i < intTahunLahir; i++) {
        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - IncrementYear'), 
            0)
    }
} else if (intCurrentYear > intTahunLahir) {
    for (int i = intCurrentYear; i > intTahunLahir; i--) {
        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - DecrementYear'), 
            0)
    }
} else {
    CustomKeywords.'set.MarkAndMessage.markPassed'('Current year dan tahun lahir sudah sama')
}

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - Ok'), 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Tempat Lahir'), tempat_lahir, 
    0)

Mobile.scrollToText('Status perkawinan', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pendidikan terakhir'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Pendidikan terakhir', 
        [('pilih_pendidikan_terakhir') : pendidikan_terakhir]), 0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Pendidikan terakhir', [('pilih_pendidikan_terakhir') : pendidikan_terakhir]), 
    0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Nama Ibu Kandung'), nama_ibu, 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Status perkawinan'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Status Perkawinan', 
        [('pilih_status') : status_perkawinan]), 0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Status Perkawinan', [('pilih_status') : status_perkawinan]), 
    0)

if (status_perkawinan == 'Menikah') {
    Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Nama Pasangan'), nama_pasangan, 
        0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Tanggal Lahir Pasangan'), 0)

    if (intCurrentMonth < intBulanLahirP) {
        for (int i = intCurrentMonth; i < intBulanLahirP; i++) {
            Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - IncrementMonth'), 
                0)
        }
    } else if (intCurrentMonth > intBulanLahirP) {
        for (int i = intCurrentMonth; i > intBulanLahirP; i--) {
            Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - DecrementMonth'), 
                0)
        }
    } else {
        CustomKeywords.'set.MarkAndMessage.markPassed'('Current month dan bulan lahir sudah sama')
    }
    
	Mobile.delay(1)
    if (intCurrentDay < intTanggalLahirP) {
        for (int i = intCurrentDay; i < intTanggalLahirP; i++) {
            Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - IncrementDay'), 
                0)
        }
    } else if (intCurrentDay > intTanggalLahirP) {
        for (int i = intCurrentDay; i > intTanggalLahirP; i--) {
            Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - DecrementDay'), 
                0)
        }
    } else {
        CustomKeywords.'set.MarkAndMessage.markPassed'('Current day dan tanggal lahir sudah sama')
    }
    
    if (intCurrentYear < intTahunLahirP) {
        for (int i = intCurrentYear; i < intTahunLahirP; i++) {
            Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - IncrementYear'), 
                0)
        }
    } else if (intCurrentYear > intTahunLahirP) {
        for (int i = intCurrentYear; i > intTahunLahirP; i--) {
            Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - DecrementYear'), 
                0)
        }
    } else {
        CustomKeywords.'set.MarkAndMessage.markPassed'('Current year dan tahun lahir sudah sama')
    }
    
    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Date of Birth/Button - Ok'), 0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pendidikan Pasangan'), 0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Pendidikan Pasangan', 
            [('pilih_pendidikan_pasangan') : pendidikan_pasangan]), 0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Pendidikan Pasangan', 
            [('pilih_pendidikan_pasangan') : pendidikan_pasangan]), 0 // Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Tanggal Lahir Pasangan'),  0)
        )

    Mobile.scrollToText('Alamat domisili', FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Jumlah Tanggungan'), 0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Jumlah Tanggungan', 
            [('pilih_jumlah_tanggungan') : jumlah_tanggungan]), 0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Jumlah Tanggungan', [
                ('pilih_jumlah_tanggungan') : jumlah_tanggungan]), 0)

    Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Alamat Domisili'), alamat_domisili, 
        0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Provinsi'), 0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Wait for Choice'), 
        0)

    Mobile.scrollToText(provinsi, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Provinsi', [('pilih_provinsi') : provinsi]), 
        0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kota'), 
        0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kota'), 0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Wait for Choice'), 
        0)

    Mobile.scrollToText(kota, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Kota', [('pilih_kota') : kota]), 
        0)

    Mobile.scrollToText('Status tempat tinggal', FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kecamatan'), 
        0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kecamatan'), 0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Wait for Choice'), 
        0)

    Mobile.scrollToText(kecamatan, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Kecamatan', [('pilih_kecamatan') : kecamatan]), 
        0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kelurahan'), 
        0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kelurahan'), 0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Wait for Choice'), 
        0)

    Mobile.scrollToText(kelurahan, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Kelurahan', [('pilih_kelurahan') : kelurahan]), 
        0)

    Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Textfield - RT'), rt, 0)

    Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Textfield - RW'), rw, 0)

    Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Telp Rumah'), no_telp, 0)

    Mobile.scrollToText('Status tempat tinggal', FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Lama menempati Rumah'), lama_menempati_rumah, 
        0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Status Tempat Tinggal'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Status Tempat Tinggal', 
            [('pilih_status_tempat_tinggal') : status_tempat_tinggal]), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Status Tempat Tinggal', 
            [('pilih_status_tempat_tinggal') : status_tempat_tinggal]), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
} else {
    boolean verify_nama_pasangan = Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Nama Pasangan'), 
        nama_pasangan, 0, FailureHandling.OPTIONAL)

    boolean verify_pendidikan_pasangan = Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pendidikan Pasangan'), 
        0, FailureHandling.OPTIONAL)

    if ((verify_nama_pasangan == false) && (verify_pendidikan_pasangan == false)) {
        CustomKeywords.'set.MarkAndMessage.markPassed'('verify success')

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Jumlah Tanggungan'), 0)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Jumlah Tanggungan', 
                [('pilih_jumlah_tanggungan') : jumlah_tanggungan]), 0)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Jumlah Tanggungan', 
                [('pilih_jumlah_tanggungan') : jumlah_tanggungan]), 0)

        Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Alamat Domisili'), alamat_domisili, 
            0)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Provinsi'), 0)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Wait for Choice'), 
            0)

        Mobile.scrollToText(provinsi, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Provinsi', [('pilih_provinsi') : provinsi]), 
            0)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kota'), 
            0)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kota'), 0)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Wait for Choice'), 
            0)

        Mobile.scrollToText(kota, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Kota', [('pilih_kota') : kota]), 
            0)

        Mobile.scrollToText('Milik sendiri', FailureHandling.STOP_ON_FAILURE)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kecamatan'), 
            0)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kecamatan'), 0)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Wait for Choice'), 
            0)

        Mobile.scrollToText(kecamatan, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Kecamatan', [('pilih_kecamatan') : kecamatan]), 
            0)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kelurahan'), 
            0)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Kelurahan'), 0)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Wait for Choice'), 
            0)

        Mobile.scrollToText(kelurahan, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Kelurahan', [('pilih_kelurahan') : kelurahan]), 
            0)

        Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Textfield - RT'), rt, 0)

        Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Textfield - RW'), rw, 0)

        Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Telp Rumah'), no_telp, 
            0)

        Mobile.scrollToText('Status tempat tinggal', FailureHandling.STOP_ON_FAILURE)

        Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Lama menempati Rumah'), 
            lama_menempati_rumah, 0)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Status Tempat Tinggal'), 0, 
            FailureHandling.STOP_ON_FAILURE)

        Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Status Tempat Tinggal', 
                [('pilih_status_tempat_tinggal') : status_tempat_tinggal]), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Selected/Selected - Status Tempat Tinggal', 
                [('pilih_status_tempat_tinggal') : status_tempat_tinggal]), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)
    } else {
        CustomKeywords.'set.MarkAndMessage.markFailedAndStop'('Data Pasangan masih bisa terisi walaupun tidak menikah - failed')
    }
}

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Selanjutnya'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Text - Informasi Pekerjaan dan Penghasilan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Text - Informasi Pekerjaan dan Penghasilan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Button - Jenis Pekerjaan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - EmployeeId'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Nama Instansi'), 
    0)

Mobile.scrollToText('No telepon kantor', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Lama Bekerja'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Alamat Kantor'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - No Telp Kantor'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Nama Atasan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Jabatan'), 
    0)

Mobile.scrollToText('Sumber pendapatan lain', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Gaji Perbulan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Pendapatan Lain'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Pekerjaan dan Penghasilan Pemohon Page/Textfield - Sumber Pendapatan Lain'), 
    0)

Mobile.scrollToText('Informasi Pekerjaan dan Penghasilan', FailureHandling.STOP_ON_FAILURE)

