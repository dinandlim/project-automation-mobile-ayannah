import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

long mathKTP = ((Math.floor(Math.random() * 100000000000000000)) as long)

String NoKTP = mathKTP

long mathNPWP = ((Math.floor(Math.random() * 1000000000000000)) as long)

String NoNPWP = mathNPWP

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Lookup - Input Foto KTP'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Input Foto KTP/Button - Take Photo'), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Input Foto KTP/Button - Take Photo'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Input Nomor KTP'), 
    0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Input Nomor KTP'), NoKTP, 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Lookup - Input Foto NPWP'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Input Foto KTP/Button - Take Photo'), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Input Foto KTP/Button - Take Photo'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Input Nomor NPWP'), 
    0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Input Nomor NPWP'), NoNPWP, 0)

Mobile.hideKeyboard()

Mobile.scrollToText('Password', FailureHandling.STOP_ON_FAILURE)

String SALTCHARS = 'abcdefghijklmnopqrstuvwxyz1234567890'

StringBuilder salt = new StringBuilder()

Random rnd = new Random()

while (salt.length() < 10) {
    // length of the random string.
    int index = ((rnd.nextFloat() * SALTCHARS.length()) as int)

    salt.append(SALTCHARS.charAt(index))
}

String saltStr = salt.toString()

GlobalVariable.email = (saltStr + '@mailinator.com')

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Email'), GlobalVariable.email, 
    0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Phone Number'), GlobalVariable.PhoneNum, 
    0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Password'), GlobalVariable.password, 
    0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Reenter Password'), GlobalVariable.password, 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Button - Selanjutnya'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Text - Data Pemohon'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Text - Data Pemohon'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Nama KTP'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Radio Btn - Laki-laki'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Radio Btn - Perempuan'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Tanggal Lahir'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Tempat Lahir'), 
    0)

Mobile.scrollToText('Status perkawinan', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pendidikan terakhir'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Nama Ibu Kandung'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Status perkawinan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Nama Pasangan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Tanggal Lahir Pasangan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pendidikan Pasangan'), 
    0)

Mobile.scrollToText('Alamat domisili', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Jumlah Tanggungan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Alamat Domisili'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Pilih Provinsi'), 0)

Mobile.scrollToText('Status tempat tinggal', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Telp Rumah'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Textfield - Lama menempati Rumah'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Pemohon Page/Button - Status Tempat Tinggal'), 
    0)

Mobile.scrollToText('Data Pemohon', FailureHandling.STOP_ON_FAILURE)

