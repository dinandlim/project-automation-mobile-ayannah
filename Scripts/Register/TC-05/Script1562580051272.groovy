import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

long math = ((Math.floor(Math.random() * 10000000000000)) as long)

String NoRek = math

Mobile.setText(findTestObject('Sign In-Sign Up Page/Register/Input Nomor Rekening Page/Textfield - Input Account Number'), 
    NoRek, 0)

Mobile.hideKeyboard()

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Input Nomor Rekening Page/Button - Selanjutnya'), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Input Nomor Rekening Page/Button - Selanjutnya'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Text - Upload kartu identitas anda'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Text - Upload kartu identitas anda'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Button - OK'), 0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Button - OK'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Input Nomor KTP'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Lookup - Input Foto KTP'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Input Nomor KTP'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Lookup - Input Foto NPWP'), 
    0)

Mobile.scrollToText('Password', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Input Nomor NPWP'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Email'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Phone Number'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Password'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Textfield - Reenter Password'), 
    0)

Mobile.scrollToText('Tambahkan foto KTP anda', FailureHandling.STOP_ON_FAILURE)

