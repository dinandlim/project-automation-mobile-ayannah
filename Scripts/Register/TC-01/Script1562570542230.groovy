import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Open Apps'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Icon - Asira'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Textfield - Mobile Phone'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Textfield - Password'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Button - Lupa Password'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Button - Masuk'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Button - Daftar'), 0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Button - Daftar'), 0)

