import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Account Number Availability Popup Page/Text - Kepemilikan Rekening'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Account Number Availability Popup Page/Text - Kepemilikan Rekening'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Account Number Availability Popup Page/Button - Tidak'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Account Number Availability Popup Page/Button - Ya'), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Account Number Availability Popup Page/Button - Tidak'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Text - Upload kartu identitas anda'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Text - Upload kartu identitas anda'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Button - OK'), 0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Register/Data Profile Page/Button - OK'), 0)

WebUI.callTestCase(findTestCase('Register/Continue TC-03'), [:], FailureHandling.STOP_ON_FAILURE)

