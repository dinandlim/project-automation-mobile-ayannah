import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
//import java.util.Random
//import java.security.SecureRandom


Integer intBulanLahir

Integer intBulanLahirP

Integer intCurrentMonth

String[] splitDateofBirth = tanggal_lahir.split(' ')

String TanggalLahir = splitDateofBirth[0]

String BulanLahir = splitDateofBirth[1]

String TahunLahir = splitDateofBirth[2]

String[] splitDateofBirth2 = tanggal_lahir_pasangan.split(' ')

String TanggalLahirP = splitDateofBirth2[0]

String BulanLahirP = splitDateofBirth2[1]

String TahunLahirP = splitDateofBirth2[2]

Integer intTanggalLahir = Integer.parseInt(TanggalLahir)

Integer intTahunLahir = Integer.parseInt(TahunLahir)

Integer intTanggalLahirP = Integer.parseInt(TanggalLahirP)

Integer intTahunLahirP = Integer.parseInt(TahunLahirP)

if (BulanLahir.equals('Jan')) {
    intBulanLahir = '1'
} else if (BulanLahir.equals('Feb')) {
    intBulanLahir = '2'
} else if (BulanLahir.equals('Mar')) {
    intBulanLahir = '3'
} else if (BulanLahir.equals('Apr')) {
    intBulanLahir = '4'
} else if (BulanLahir.equals('May')) {
    intBulanLahir = '5'
} else if (BulanLahir.equals('Jun')) {
    intBulanLahir = '6'
} else if (BulanLahir.equals('Jul')) {
    intBulanLahir = '7'
} else if (BulanLahir.equals('Aug')) {
    intBulanLahir = '8'
} else if (BulanLahir.equals('Sep')) {
    intBulanLahir = '9'
} else if (BulanLahir.equals('Oct')) {
    intBulanLahir = '10'
} else if (BulanLahir.equals('Nov')) {
    intBulanLahir = '11'
} else if (BulanLahir.equals('Dec')) {
    intBulanLahir = '12'
}

//Untuk bulan lahir Pasangan
if (BulanLahir.equals('Jan')) {
    intBulanLahirP = '1'
} else if (BulanLahirP.equals('Feb')) {
    intBulanLahirP = '2'
} else if (BulanLahirP.equals('Mar')) {
    intBulanLahirP = '3'
} else if (BulanLahirP.equals('Apr')) {
    intBulanLahirP = '4'
} else if (BulanLahirP.equals('May')) {
    intBulanLahirP = '5'
} else if (BulanLahirP.equals('Jun')) {
    intBulanLahirP = '6'
} else if (BulanLahirP.equals('Jul')) {
    intBulanLahirP = '7'
} else if (BulanLahirP.equals('Aug')) {
    intBulanLahirP = '8'
} else if (BulanLahirP.equals('Sep')) {
    intBulanLahirP = '9'
} else if (BulanLahirP.equals('Oct')) {
    intBulanLahirP = '10'
} else if (BulanLahirP.equals('Nov')) {
    intBulanLahirP = '11'
} else if (BulanLahirP.equals('Dec')) {
    intBulanLahirP = '12'
}

CurrentMonth = "Aug"

CurrentDay = "02"

CurrentYear = "2019"

if (CurrentMonth.equals('Jan')) {
	intCurrentMonth = '1'
} else if (CurrentMonth.equals('Feb')) {
	intCurrentMonth = '2'
} else if (CurrentMonth.equals('Mar')) {
	intCurrentMonth = '3'
} else if (CurrentMonth.equals('Apr')) {
	intCurrentMonth = '4'
} else if (CurrentMonth.equals('May')) {
	intCurrentMonth = '5'
} else if (CurrentMonth.equals('Jun')) {
	intCurrentMonth = '6'
} else if (CurrentMonth.equals('Jul')) {
	intCurrentMonth = '7'
} else if (CurrentMonth.equals('Aug')) {
	intCurrentMonth = '8'
} else if (CurrentMonth.equals('Sep')) {
	intCurrentMonth = '9'
} else if (CurrentMonth.equals('Oct')) {
	intCurrentMonth = '10'
} else if (CurrentMonth.equals('Nov')) {
	intCurrentMonth = '11'
} else if (CurrentMonth.equals('Dec')) {
	intCurrentMonth = '12'
}



Integer intCurrentDay = Integer.parseInt(CurrentDay)

Integer intCurrentYear = Integer.parseInt(CurrentYear)

print(intCurrentMonth)
print(intBulanLahir)

if (intCurrentMonth < intBulanLahir) {
	for (int i = intCurrentMonth; i < intBulanLahir; i++) {
		print("hit tambah")
	}
} else if (intCurrentMonth > intBulanLahir) {
	for (int i = intCurrentMonth; i > intBulanLahir; i--) {
		print("hit kurang")
	}
} else {
	CustomKeywords.'set.MarkAndMessage.markPassed'('Current month dan bulan lahir sudah sama')
}
