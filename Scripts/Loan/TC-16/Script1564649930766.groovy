import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('Loan/Income Update Pop Up Page')

String phone = GlobalVariable.PhoneNum

def lbl_PhoneNum = ('\'' + phone) + '\''

data.query = data.query.replace('\'_PHONE_\'', lbl_PhoneNum)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    dbMonthly_income = data.getValue(1, index)

    dbOther_income = data.getValue(2, index)

    dbSource_otherincome = data.getValue(3, index)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Button - Back'), 0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Button - Back'), 0)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Income Update Popup Page/Icon - Income Update Popup'), 
        0)

    Monthly_income_1 = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Income Update Popup Page/Text - Monthly Income'), 
        0)

    Other_income_1 = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Income Update Popup Page/Text - Other Income'), 
        0)

    newtxtSource_otherincome = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Income Update Popup Page/Text - Source of Other Income'), 
        0)

    //Menghilangkan "Rp" dan "." dengan fungsi replace
    Monthly_income_2 = Monthly_income_1.replace('Rp', '')

    newtxtMonthly_income = Monthly_income_2.replace('.', '')

    Other_income_2 = Other_income_1.replace('Rp', '')

    newtxtOther_income = Other_income_2.replace('.', '')

    Mobile.verifyMatch(newtxtMonthly_income, dbMonthly_income, false)

    Mobile.verifyMatch(newtxtOther_income, dbOther_income, false)

    Mobile.verifyMatch(newtxtSource_otherincome, dbSource_otherincome, false)
 
}
Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Income Update Popup Page/Button - Tidak'), 0)
