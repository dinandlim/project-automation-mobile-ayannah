import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Income Update Popup Page/Button - Tidak'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/TitleText - Loan'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/TitleText - Loan'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Plafond'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Plafond Amount'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Tenor'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Tenor Amount'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Admin Fee'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Admin Fee Amount'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Interest'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Interest Amount'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Monthly Repayment'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Monthly Repayment Amount'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Alasan Meminjam'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Loan Request Reason'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Textfield - Loan Detail Purpose'), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Button - Back'), 0)

