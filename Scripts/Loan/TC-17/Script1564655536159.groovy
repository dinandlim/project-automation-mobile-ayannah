import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Deklarasi perhitungan
intPlafond = Integer.parseInt(plafond)

intTenor = Integer.parseInt(tenor)

valuePlafond = (((intPlafond - 5000000) / 5000000) * 11)

valueTenor = (((intTenor - 6) / 6) * 11)

int interest = (intPlafond * 1.5) / 100

strInterest = Integer.toString(interest)

double txtmonthly_repayment = ((intPlafond + interest) + 1000) / intTenor

int monthly_repayment = Math.ceil(txtmonthly_repayment)

strMonthly_repayment = Integer.toString(monthly_repayment)

//Continue TestCase
Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Plafond'), 0)

Mobile.setSliderValue(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Seekbar - Plafond'), valuePlafond, 
    0, FailureHandling.OPTIONAL)

Mobile.setSliderValue(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Seekbar - Tenor'), valueTenor, 0, 
    FailureHandling.OPTIONAL)

//Deklarasi untuk bunga yang diharapkan
txtInterest = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Interest Amount'), 
    0)

txtInterest1 = txtInterest.replace('Rp', '')

expectedInterest = txtInterest1.replace('.', '')

//Deklarasi untuk angsuran yang diharapkan
txtMonthlyRepayment = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Monthly Repayment Amount'), 
    0)

txtMonthlyRepayment1 = txtMonthlyRepayment.replace('Rp', '')

expectedMonthlyRepayment = txtMonthlyRepayment1.replace('.', '')

//TestCase Continue
Mobile.verifyMatch(strInterest, expectedInterest, false)

//Mobile.verifyMatch(strMonthly_repayment, expectedMonthlyRepayment, false)
Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Button - Alasan Pinjam'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Pendidikan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Pendidikan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Pembelian rumah'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Rumah tangga'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Liburan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Kendaraan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Umroh'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Lain-lain'), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Selected - Loan Request Reason', [('pilih_alasan_pinjam') : loan_req_reason]), 
    0)

actPlafond = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Plafond Amount'), 0)

actTenor = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Tenor Amount'), 0)

actInterest = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Interest Amount'), 
    0)

actAdminFee = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Admin Fee Amount'), 
    0)

actMonthlyRepayment = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Monthly Repayment Amount'), 
    0)

actLoanReason = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Loan Request Reason'), 
    0)

actLoanDetailPurpose = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Textfield - Loan Detail Purpose'), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Button - Process'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/TextTitle - Loan Request Review'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/TextTitle - Loan Request Review'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Plafond'), 0)

expPlafond = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Plafond Amount'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Tenor'), 0)

expTenor = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Tenor Amount'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Interest'), 0)

expInterest = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Interest Amount'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Admin Fee'), 0)

expAdminFee = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Admin Fee Amount'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Monthly Repayment'), 
    0)

expMonthlyRepayment = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Monthly Repayment Amount'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Loan Reason'), 0)

expLoanReason = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Inserted Loan Reason'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Loan Detail Purpose'), 
    0)

expLoanDetailPurpose = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Inserted Loan Detail Purpose'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Text - Disclaimer'), 0)

Mobile.verifyMatch(actPlafond, expPlafond, false)

Mobile.verifyMatch(actTenor, expTenor, false)

Mobile.verifyMatch(actInterest, expInterest, false)

Mobile.verifyMatch(actAdminFee, expAdminFee, false)

Mobile.verifyMatch(actMonthlyRepayment, expMonthlyRepayment, false)

Mobile.verifyMatch(actLoanReason, expLoanReason, false)

if (actLoanDetailPurpose == 'Tuliskan detail tujuan') {
    expLoanDetailPurpose == '-'
} else {
    Mobile.verifyMatch(actLoanDetailPurpose, expLoanDetailPurpose, false)
}

Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Review Page/Button - Back'), 0)

