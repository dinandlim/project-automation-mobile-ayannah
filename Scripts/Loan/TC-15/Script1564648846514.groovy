import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.setText(findTestObject('Sign In-Sign Up Page/Loan/Income Update Page/Textfield - Monthly Income'), monthly_income, 
    0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Loan/Income Update Page/Textfield - Other Income'), other_income, 0)

Mobile.setText(findTestObject('Sign In-Sign Up Page/Loan/Income Update Page/Textfield - Source of Other Income'), source_other_income, 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Income Update Page/Button - Selanjutnya'), 0, FailureHandling.STOP_ON_FAILURE)

