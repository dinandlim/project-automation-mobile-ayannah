import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil

intPlafond = Integer.parseInt(plafond)

intTenor = Integer.parseInt(tenor)

valuePlafond = (((intPlafond - 5000000) / 5000000) * 11)

valueTenor = (((intTenor - 6) / 6) * 11)

int interest = (intPlafond * 1.5) / 100

strInterest = Integer.toString(interest)

double txtmonthly_repayment = ((intPlafond + interest) + 1000) / intTenor

int monthly_repayment = Math.ceil(txtmonthly_repayment)

strMonthly_repayment = Integer.toString(monthly_repayment)

//Continue TestCase
Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Plafond'), 0)

Mobile.setSliderValue(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Seekbar - Plafond'), valuePlafond, 
    0, FailureHandling.OPTIONAL)

Mobile.setSliderValue(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Seekbar - Tenor'), valueTenor, 0, 
    FailureHandling.OPTIONAL)

//Deklarasi untuk bunga yang diharapkan
txtInterest = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Interest Amount'), 
    0)

txtInterest1 = txtInterest.replace('Rp', '')

expectedInterest = txtInterest1.replace('.', '')

//Deklarasi untuk angsuran yang diharapkan
txtMonthlyRepayment = Mobile.getText(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Monthly Repayment Amount'), 
    0)

txtMonthlyRepayment1 = txtMonthlyRepayment.replace('Rp', '')

expectedMonthlyRepayment = txtMonthlyRepayment1.replace('.', '')

//TestCase Continue
Mobile.verifyMatch(strInterest, expectedInterest, false)

//Mobile.verifyMatch(strMonthly_repayment, expectedMonthlyRepayment, false)
Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Button - Alasan Pinjam'), 0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Pendidikan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Pendidikan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Pembelian rumah'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Rumah tangga'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Liburan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Kendaraan'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Umroh'), 
    0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Dropdown List/Dropdown List - Lain-lain'), 
    0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Selected - Loan Request Reason', [('pilih_alasan_pinjam') : loan_req_reason]), 
    0)

Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Button - Process'), 0)

Mobile.tap(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Button - Process'), 0)

AppiumDriver<?> driver = MobileDriverFactory.getDriver()
boolean toast = driver.findElementByXPath("//android.widget.Toast[@text='Mohon diisi alasannya']")
println("Toast element: " + toast)
if (toast == false) {
	KeywordUtil.markFailed('ERROR: Toast object not found!')
}

//Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Loan Request Form Page/Text - Message Reason Empty'), 
  //  0)

