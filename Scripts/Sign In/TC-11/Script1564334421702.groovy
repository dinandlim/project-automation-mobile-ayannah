import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.DBData as DBData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DBData data = findTestData('Sign In/Sign In Page')

String phone = GlobalVariable.PhoneNum

def lbl_PhoneNum = ('\'' + phone) + '\''

data.query = data.query.replace('\'_PHONE_\'', lbl_PhoneNum)

data.fetchedData = data.fetchData()

for (def index : (1..data.getRowNumbers())) {
    dbPhone = data.getValue(1, index)

    Mobile.waitForElementPresent(findTestObject('Sign In-Sign Up Page/Textfield - Mobile Phone'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('Sign In-Sign Up Page/Textfield - Mobile Phone'), dbPhone, 0)

    Mobile.setText(findTestObject('Sign In-Sign Up Page/Textfield - Password'), GlobalVariable.password, 0)

    Mobile.tap(findTestObject('Sign In-Sign Up Page/Button - Masuk'), 0)
}

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Landing Page/Icon - Asira'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Landing Page/Button - Pinjaman PNS'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Landing Page/Button - More Option'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Landing Page/Text - News and Promo'), 0)

Mobile.verifyElementVisible(findTestObject('Sign In-Sign Up Page/Loan/Landing Page/Icon - News and Promo'), 0)

