<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button - IncrementMonth</name>
   <tag></tag>
   <elementGuidId>4fa6281a-742e-479b-a6ba-88804dc4027d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'android.widget.LinearLayout' and @resource-id = 'android:id/month']/*[@class = 'android.widget.ImageButton' and @resource-id = 'android:id/increment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.ImageButton</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>android:id/increment</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>com.ayannah.bantenbank</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>content-desc</name>
      <type>Main</type>
      <value>Increase month</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class = 'android.widget.LinearLayout' and @resource-id = 'android:id/month']/*[@class = 'android.widget.ImageButton' and @resource-id = 'android:id/increment']</value>
   </webElementProperties>
</WebElementEntity>
